import React from 'react';
import Article from './../Article/Article';


export default class Posts extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            posts: []
        }
    }

    async componentDidMount() {
        // fetch('https://jsonplaceholder.typicode.com/posts')
        //     .then((response) => response.json())
        //     .then((data) => this.setState({ posts: data }));

        try {
            const respuesta = await fetch('https://jsonplaceholder.typicode.com/posts');
            console.log('respon', respuesta);
            const data = await respuesta.json();
            const dataConMenos = data.slice(0,10);
            this.setState({ posts: dataConMenos});
            console.log(dataConMenos);
        } catch (e) {
            console.log('error', e)
        }
        
    }

    render() {
        return (
            <div className="row">
                {this.state.posts.map((item) => (
                    <Article data={item} />
                ))}
            </div>
        );
    }

}



