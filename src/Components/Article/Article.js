import React from 'react';
import { Link } from 'react-router-dom';

export default class Article extends React.Component {
    render(){
      return (
        <div className="col-4">
            <div class="card">
                <div class="card-body"> 
                    <h5 class="card-title">{this.props.data.title}</h5>
                    <p class="card-text">{this.props.data.body}</p>
                    <Link to={'/posts/' + this.props.data.id} className="btn btn-primary">Go somewhere</Link>
                </div>
            </div>
        </div>
      );
    }
    
  }
  