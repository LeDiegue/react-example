import React, { useState, useEffect }  from 'react';
import { useParams } from 'react-router-dom';

export default function Post(props) {
    const { id } = useParams(); // const id = userParams().id;
    const [post, setPost] = useState({});

    useEffect(() => {
        // componentDidMount
        // componentWillMount()
        fetch('https://jsonplaceholder.typicode.com/posts/' + id)
            .then((response) => response.json())
            .then((data) => setPost(data));
        

        console.log('ahora si');

        return () => {
            //componentWillUnmount
            console.log('ya me voy');
        }
    })

    return (
        <div className="row">
            <h2>{post.title}</h2>
            <p>{post.body}</p>
        </div>
    );

}
